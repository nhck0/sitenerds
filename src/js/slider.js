var slider = document.querySelector(".slider");
var slides = document.querySelectorAll(".slider__item");
var slidesNav = document.querySelector(".slider-nav");
var slidesButtons = document.querySelectorAll(".slider-nav__button");


slidesNav.addEventListener("click", function (evt) {
    for(var i = 0; i < slidesButtons.length; i++){
        if(evt.target === slidesButtons[i]){
            for(var buttonNum = 0; buttonNum < slidesButtons.length; buttonNum++){
                slidesButtons[buttonNum].classList.remove("slider-nav__button--current");
            }
            for(var slideNum = 0; slideNum < slides.length; slideNum++){
                slides[slideNum].classList.remove("slider__item--current");
            }
            slidesButtons[i].classList.add("slider-nav__button--current");
            slides[i].classList.add("slider__item--current");

            switch (i){
                case 0:
                    slider.classList.remove("slider--bg2");
                    slider.classList.remove("slider--bg3");
                    slider.classList.add("slider--bg1");
                    break;
                case 1:
                    slider.classList.remove("slider--bg1");
                    slider.classList.remove("slider--bg3");
                    slider.classList.add("slider--bg2");
                    break;
                case 2:
                    slider.classList.remove("slider--bg1");
                    slider.classList.remove("slider--bg2");
                    slider.classList.add("slider--bg3");
                    break;
                default:
                    slider.classList.add("slider--bg1");
            }
        }
    }
});