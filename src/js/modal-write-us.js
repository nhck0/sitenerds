var overlay = document.querySelector(".overlay");

var popupForm = document.querySelector(".modal-write-us");
var popupFormBtn = document.querySelector(".write-us__btn");
var popupCloseBtn = document.querySelector(".modal-close");
var popupInputs = document.querySelectorAll(".write-us-form__input");
var popupTextarea = document.querySelector(".write-us-form__textarea");

var storageName = localStorage.getItem("login");
var storageMail = localStorage.getItem("email");
var isStorageSupport = true;

try{
    storageName = localStorage.getItem("login");
    storageName = localStorage.getItem("email");
} catch (err){
    isStorageSupport = false;
}

popupFormBtn.addEventListener("click", function (evt) {
   evt.preventDefault();
   popupForm.classList.add("modal-show");
   overlay.classList.add("overlay--on");
   if(isStorageSupport){
       popupInputs[0].value = storageName;
       popupInputs[1].value = storageMail;
   }

   for(var i=0; i<popupInputs.length; i++){
      if(!popupInputs[i].value){
         popupInputs[i].focus();
         break;
      } else{
         popupTextarea.focus();
      }
   }

   // for(var i = 0; i < popupInputs.length; i++){
   //     popupInputs[i].classList.remove("write-us-form__input--required");
   // }
   // popupTextarea.classList.remove("write-us-form__input--required");

});

popupCloseBtn.addEventListener("click", function (evt) {
   evt.preventDefault();
   if(popupForm.classList.contains("modal-show")){
      popupForm.classList.remove("modal-show");
      overlay.classList.remove("overlay--on");
   }
});

window.addEventListener("keydown", function (evt) {
   if(evt.keyCode === 27){
       evt.preventDefault();
       popupForm.classList.remove("modal-show");
       overlay.classList.remove("overlay--on");
   }
});

popupForm.addEventListener("submit", function (evt) {
    for(var i = 0; i < popupInputs.length; i++){
        if(!popupInputs[i].value){
            evt.preventDefault();
            // popupInputs[i].classList.add("write-us-form__input--required");
        } else{
            storageName = localStorage.setItem("login", popupInputs[0].value);
            storageMail = localStorage.setItem("email", popupInputs[1].value);
        }
    }
    // if(!popupTextarea.value){
    //     evt.preventDefault();
    //     popupTextarea.classList.add("write-us-form__input--required");
    // }
});

overlay.addEventListener("click", function () {
   if(overlay.classList.contains("overlay--on")){
       popupForm.classList.remove("modal-show");
       overlay.classList.remove("overlay--on");
   }
});
