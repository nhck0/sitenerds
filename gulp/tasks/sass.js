'use strit'

module.exports = function() {
	$.gulp.task('sass', function(){
		return $.gulp.src('./src/styles/main.scss')
			.pipe($.gp.sourcemaps.init())
				.pipe($.gp.sass())
				.on('error', $.gp.notify.onError(function(error){
					return{
						title: 'Styles',
						message: error.message
					}
				}))
				.pipe($.gp.autoprefixer({
					browsers: [
						'last 3 version',
						'> 1%',
						'ie 8',
						'ie 9'
					]
				}))
				.pipe($.cssunit({
					type: 'px-to-rem',
					rootSize: 16
				}))
				.pipe($.gp.csso())
			.pipe($.gp.sourcemaps.write())
			.pipe($.gp.rename('main.min.css'))
			.pipe($.gulp.dest('./build/css'))
	});
};


