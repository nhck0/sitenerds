'use strict';

module.exports = function(){
	$.gulp.task('bower', function(){
		return $.gulp.src('./build/*.html')
		.pipe($.gp.wiredep({
			directory : "./src/bower_components"
		}))
		.on('error', $.gp.notify.onError(function(error){
			return{
				title: 'Bower',
				message: error.message
			}
		}))
		.pipe($.gulp.dest('./build/'))
	});
};