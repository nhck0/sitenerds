'use strict'

module.exports = function(){
	$.gulp.task('watch', function(){
		$.gulp.watch('./src/styles/**/*.{css,scss}', $.gulp.series('sass'));
		$.gulp.watch('./src/fonts/**/*.*', $.gulp.series('fonts'));
		$.gulp.watch('./src/assets/**/*.*', $.gulp.series('sprite', 'svgSpriteBuild'));
		$.gulp.watch(['./bower.json', './src/template/**/*.pug', './src/js/*.js'], $.gulp.series('pug', 'bower', 'useref'));
	});
};