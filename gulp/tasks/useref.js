'use strict';

module.exports = function () {
	$.gulp.task('useref', function() {
		return $.gulp.src('./build/*.html')
		.pipe($.gp.useref({
			searchPath: './src/'
		}))
		.on('error', $.gp.notify.onError(function(error){
			return{
				title: 'useref',
				message: error.message
			}
		}))
		.pipe($.gp.if('*.js', $.gp.uglify()))
		.pipe($.gp.if('*.css', $.gp.csso()))
		.pipe($.gulp.dest('./build'))
	});
};